//phone mask
$("#phone").mask("+7 (999) 999-9999");

var info = document.querySelector('.form__input-info');
var error = document.querySelector('.form__input-info_error');
var modalBtns = document.querySelectorAll(".modalButton");
var modalCloseBtns = document.querySelectorAll(".modal__close");
var prodPics = document.querySelectorAll('.products__pic');
var form = document.querySelector('.form');
var formInput = document.querySelector('.form__input');

modalBtns.forEach(function(btn){
  btn.onclick = function() {
    var modal = btn.getAttribute('data-modal');
    document.getElementById(modal).style.display = "flex";
    prodPics.forEach(function(item){
      item.classList.toggle('products__pic_disabled');
    })
  }
});

modalCloseBtns.forEach(function(btn){
  btn.onclick = function() {
    var modal = btn.closest('.modal');
    modal.style.display = "none";
    prodPics.forEach(function(item){
      item.classList.toggle('products__pic_disabled');
    })
  }
});

var formValidate = function(){
  return (formInput.value) ? formValidateSuccess() : formValidateError();
}

var formValidateSuccess = function(){
  document.querySelector('.form__submit').classList.add('form__submit_disabled');
  document.querySelector('.form__container').style.display = 'none';
  document.querySelector('.form__container_sent').style.display = 'block';
}

var formValidateError = function(){
  formInput.classList.add('form__input_error');
  info.classList.add('invisible');
  error.classList.add('visible');
}

formInput.addEventListener('focus', function(event){
  formInput.classList.remove('form__input_error');
  formInput.classList.add('form__input_focus');
  info.classList.remove('invisible');
  error.classList.remove('visible');
})

formInput.addEventListener('blur', function(event){
  formInput.classList.remove('form__input_focus');
})

form.addEventListener('submit', function(event){
  event.preventDefault();
  return (formValidate()) ? this.submit() : false;
})
